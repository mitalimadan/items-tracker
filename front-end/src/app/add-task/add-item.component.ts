import {Component, OnInit} from '@angular/core';
import {User} from '../model/user';
import {FormControl, FormGroup} from '@angular/forms';
import {ItemServiceService} from '../itemService.service';
import {Project} from '../model/project';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {


  itemType: ItemType;
  keys = [];
  userList: User[];
  projectList: Project[];
  itemForm: FormGroup;

  constructor(private itemService: ItemServiceService) {
    //this.keys = Object.keys(ItemType);
    this.itemForm = new FormGroup({
      name: new FormControl(),
      description: new FormControl(),
      parent: new FormControl(),
      type: new FormControl(),
      priority: new FormControl(),
      estimatedTime: new FormControl(),
      createdBy: new FormControl(),
      linkedProject: new FormControl(),

    });
  }

  ngOnInit() {
    this.getAllUserAndProjectInfo();
  }

  addItem() {
    const item = this.itemForm.value;
    this.itemService
      .addItem(item)
      .subscribe((response) => {
        alert('Added Successfully');
      });


  }

  getAllUserAndProjectInfo() {
    this.itemService
      .getUserAndProjectInfo()
      .subscribe((response) => {
        console.log(response);

        this.userList = response.user;

        this.projectList = response.project;
        console.log("Hello");
      });
  }
}
