import {User} from './user';
import {Project} from './project';
/**
 * Created by Lucky  on 13/05/2020.
 */
export class UserProjectInfo {

  user: User[];
  project: Project[];

}
