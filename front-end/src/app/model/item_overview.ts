/**
 * Created by Lucky  on 11/05/2020.
 */
export class ItemOverview {
  name: string;
  description: string;
  type: number;
}
