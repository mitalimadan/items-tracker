/**
 * Created by Lucky  on 12/05/2020.
 */
export class User {
  id: number;
  name: string;
  email: string;
  password: string;
  type: number;
}
