import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {Item} from './model/item';
import {User} from './model/user';

@Injectable({
  providedIn: 'root'
})
export class ItemServiceService {


  private baseUrl = 'http://localhost:8089/items';


  constructor(private http: HttpClient) {
  }

  getItemList(): Observable<any> {
    var observable = this.http.get(`${this.baseUrl}`);
    return observable;
  }


  addItem(item: Item): Observable<any> {
    return this.http.post(`${this.baseUrl}`, item);
  }

  getUserAndProjectInfo(): Observable<any> {
    var observable = this.http.get('http://localhost:8089/usersAndProjects');
    return observable;
  }


  signUpInfo(user:User): Observable<any> {
    var observable = this.http.post('http://localhost:8089/users',user);
    return observable;
  }


}
