import {Component, OnInit} from '@angular/core';
import {Item} from 'src/app/model/item';
import {ItemServiceService} from '../itemService.service';
import {ItemOverview} from '../model/item_overview';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.css']
})
export class ViewItemComponent implements OnInit {

  items: Item[];
  itemOverviews: ItemOverview[];
  constructor(private taskService: ItemServiceService) {
  }

  ngOnInit() {
    this.getAllTask();
    this.itemOverviews = [];

  }

  getAllTask() {
    this.taskService
      .getItemList()
      .subscribe((response) => {
          this.items = response;
        this.items.map((item) => {
          var itemOverView = {
            name: item.name,
            description: item.description,
            type: item.type
          };
          this.itemOverviews.push(itemOverView);
        })
        debugger;
      });


  }

}
