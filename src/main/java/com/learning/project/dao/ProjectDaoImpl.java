package com.learning.project.dao;

import com.learning.project.model.Project;
import com.learning.project.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.learning.project.util.ProjectRowMapper.PROJECT_ROW_MAPPER;
import static com.learning.project.util.UserRowMapper.USER_ROW_MAPPER;

/**
 * Created by Lucky  on 12/05/2020.
 */
@Repository
public class ProjectDaoImpl implements ProjectDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Override
    public List<Project> getAllProjects() {

        List<Project> projects = jdbcTemplate.query("select * from project", PROJECT_ROW_MAPPER);
        return projects;
    }

    public Project getProjectById(int id) {
        String sql = "select * from project where id=:id";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", id);
        Project project = null;
        try {
            project = namedParameterJdbcTemplate.queryForObject(sql, namedParameters, PROJECT_ROW_MAPPER);
        } catch (EmptyResultDataAccessException e) {
            System.out.println("No data found for " + id);
        }

        return project;

    }
}
