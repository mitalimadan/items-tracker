package com.learning.project.dao;

import com.learning.project.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.learning.project.util.Utility.ITEM_ROW_MAPPER;

/**
 * Created by Administrator on 10-11-2019.
 */

@Repository
public class ItemDaoImpl implements ItemDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    private static final String ITEM_UPDATE_SQL = "update item set name = ?,description=?,parent=?,type=?,priority=?,created_at=?,created_by=?,estimate_time=?,linked_project=? where id = ?";
    private static final String ITEM_DELETE_SQL = "delete from item where id = ?";
    String INSERT_ITEM_SQL
            = "insert into item (name, description, parent, type, priority, created_at, created_by, estimate_time," +
            "project_linked) values(?,?,?,?,?,?,?,?,?) ";

/*

    @Override
    public boolean addItem(Item item) {
        boolean isAdded = false;
        String sql = "insert into  item(name, description, parent, type, priority, created_at, created_by, estimate_time,project_linked)" +
                "values(:name, :description, :parent, :type, :priority, NOW(), :created_by,:estimate_time,:project_linked)";


        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name", item.getName());
        paramMap.put("description", item.getDescription());
        paramMap.put("parent", item.getParent());
        paramMap.put("type", item.getType());
        paramMap.put("priority", item.getPriority());
        paramMap.put("created_by", item.getCreatedBy());
        paramMap.put("estimate_time", item.getEstimatedTime());
        paramMap.put("project_linked", item.getProjectLinked());


        int noOfRowsInserted = namedParameterJdbcTemplate.update(sql, paramMap);
        if (noOfRowsInserted > 0) {
            isAdded = true;
        }
        return isAdded;
    }
*/

    public int addItem(Item item) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(INSERT_ITEM_SQL);
            ps.setString(1, item.getName());
            ps.setString(2, item.getDescription());
            ps.setInt(3, item.getParent());
            ps.setInt(4, item.getType());
            ps.setInt(5, item.getPriority());
            ps.setDate(6, item.getCreatedAt());
            ps.setInt(7, item.getCreatedBy());
            ps.setFloat(8, item.getEstimatedTime());
            ps.setInt(9, item.getProjectLinked());
            return ps;
        }, keyHolder);

        return (int) keyHolder.getKey();
    }


    @Override
    public Item getItemById(int id) {
        String sql = "select * from item where id=:id";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", id);
        Item item = null;
        try {
            item = namedParameterJdbcTemplate.queryForObject(sql, namedParameters, ITEM_ROW_MAPPER);
        } catch (EmptyResultDataAccessException e) {
            System.out.println("No data found for " + id);
        }

        return item;

    }

    @Override
    public List<Item> getAllItems() {
        List<Item> items = jdbcTemplate.query("select * from item", ITEM_ROW_MAPPER);
        return items;
    }

    @Override
    public boolean updateItem(Item item) {
        boolean isUpdated = false;
        int noOfRowsUpdated = jdbcTemplate.update(ITEM_UPDATE_SQL, item.getName(), item.getDescription(), item.getParent(), item.getType(), item.getPriority(), item.getCreatedAt(), item.getCreatedBy(), item.getEstimatedTime(), item.getProjectLinked(), item.getId());
        if (noOfRowsUpdated > 0) {
            isUpdated = true;
        }
        return isUpdated;
    }

    @Override
    public boolean deleteItemById(int id) {
        boolean isDeleted = false;
        int noOfRowsDeleted = jdbcTemplate.update(ITEM_DELETE_SQL, id);
        if (noOfRowsDeleted > 0) {
            isDeleted = true;
        }
        return isDeleted;
    }


}
