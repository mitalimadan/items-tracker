package com.learning.project.dao;

import com.learning.project.model.Item;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 10-11-2019.
 */

@Repository
public interface ItemDao {

    int addItem(Item item);

    Item getItemById(int id);

    List<Item> getAllItems();

    boolean updateItem(Item item);

    boolean deleteItemById(int id);


}
