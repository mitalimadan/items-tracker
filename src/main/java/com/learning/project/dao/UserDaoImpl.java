package com.learning.project.dao;

import com.learning.project.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.learning.project.util.UserRowMapper.USER_ROW_MAPPER;

/**
 * Created by Administrator on 10-11-2019.
 */

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    private static final String USER_UPDATE_SQL = "update user set name = ?,email=?,password=?,type=? where id = ?";
    private static final String USER_DELETE_SQL = "delete from user where id = ?";

    @Override
    public boolean addUser(User user) {
        boolean isAdded = false;
        String sql = "insert into  user(name,password,email,type) values(?,?,?,?)";
        int noOfRowsInserted = jdbcTemplate.update(sql, user.getName(), user.getPassword(), user.getEmail(), user.getType());
        if (noOfRowsInserted > 0) {
            isAdded = true;
        }
        return isAdded;
    }

    @Override
    public User getUserById(int id) {
        String sql = "select * from user where id=:id";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", id);
        User user = null;
        try {
            user = namedParameterJdbcTemplate.queryForObject(sql, namedParameters, USER_ROW_MAPPER);
        } catch (EmptyResultDataAccessException e) {
            System.out.println("No data found for " + id);
        }

        return user;

    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = jdbcTemplate.query("select * from user", USER_ROW_MAPPER);
        return users;
    }

    @Override
    public boolean updateUser(User user) {
        boolean isUpdated = false;
        int noOfRowsUpdated = jdbcTemplate.update(USER_UPDATE_SQL, user.getName(), user.getEmail(), user.getPassword(), user.getType(), user.getId());
        if (noOfRowsUpdated > 0) {
            isUpdated = true;
        }
        return isUpdated;
    }

    @Override
    public boolean deleteUserById(int id) {
        boolean isDeleted = false;
        int noOfRowsDeleted = jdbcTemplate.update(USER_DELETE_SQL, id);
        if (noOfRowsDeleted > 0) {
            isDeleted = true;
        }
        return isDeleted;
    }

    @Override
    public User findUserByEmail(String email) {
        String sql = "select * from user where email=:email";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("email", email);
        User user = null;
        try {
            user = namedParameterJdbcTemplate.queryForObject(sql, namedParameters, USER_ROW_MAPPER);
        } catch (EmptyResultDataAccessException e) {
            System.out.println("No data found for " + email);
        }

        return user;

    }


}
