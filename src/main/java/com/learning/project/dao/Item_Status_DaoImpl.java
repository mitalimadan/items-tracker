package com.learning.project.dao;

import com.learning.project.model.Item_Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Lucky  on 17/05/2020.
 */
public class Item_Status_DaoImpl implements Item_Status_Dao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public boolean addItemStatus(Item_Status item_status) {
        boolean isAdded = false;
        String sql = "insert into item_status(assigned_to,status,updated_at,item_id)values(?,?,?,?)";
        int noOfRowsInserted = jdbcTemplate.update(sql, item_status.getAssignedTo(), item_status.getStatus(), item_status.getUpdatedAt(), item_status.getItemId());
        if (noOfRowsInserted > 0) {
            isAdded = true;
        }
        return isAdded;
    }
}

