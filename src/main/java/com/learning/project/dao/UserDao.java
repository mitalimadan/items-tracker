package com.learning.project.dao;

import com.learning.project.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lucky  on 12/05/2020.
 */
@Repository
public interface UserDao {

    boolean addUser(User user);

    User getUserById(int id);

    List<User> getAllUsers();

    boolean updateUser(User user);

    boolean deleteUserById(int id);

    User findUserByEmail(String email);
}
