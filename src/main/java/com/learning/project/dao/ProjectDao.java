package com.learning.project.dao;

import com.learning.project.model.Project;
import com.learning.project.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lucky  on 12/05/2020.
 */
@Repository
public interface ProjectDao {
    List<Project> getAllProjects();

    Project getProjectById(int id);
}
