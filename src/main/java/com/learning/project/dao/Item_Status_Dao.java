package com.learning.project.dao;

import com.learning.project.model.Item_Status;

/**
 * Created by Lucky  on 17/05/2020.
 */
public interface Item_Status_Dao {

    boolean addItemStatus(Item_Status item_status);
}
