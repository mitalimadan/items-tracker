package com.learning.project.service;

import com.learning.project.dao.UserDao;
import com.learning.project.model.User;
import com.learning.project.model.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Lucky  on 15/05/2020.
 */
@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserDao userDao;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User userByEmail = userDao.findUserByEmail(email);
        if (userByEmail == null) {
            throw new UsernameNotFoundException("User 404");
        }
        UserPrincipal userPrincipal = new UserPrincipal(userByEmail);
        return userPrincipal;
    }


}
