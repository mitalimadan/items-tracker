package com.learning.project.service;

import com.learning.project.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Lucky  on 12/05/2020.
 */
@Service
public interface UserService {
    boolean addUser(User user);

    User getUserById(int id);

    List<User> getAllUsers();

    boolean updateUser(User user);

    boolean deleteUserById(int id);
}
