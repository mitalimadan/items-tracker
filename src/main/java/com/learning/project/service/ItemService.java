package com.learning.project.service;

import com.learning.project.model.Item;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 03-12-2019.
 */

@Service
public interface ItemService {
   int addItem(Item item);

   Item getItemById(int id);

   List<Item> getAllItems();

   boolean updateItem(Item item);

   boolean deleteItemById(int id);

}
