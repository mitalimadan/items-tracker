package com.learning.project.service;

import com.learning.project.dao.ItemDao;
import com.learning.project.dao.ProjectDao;
import com.learning.project.dao.UserDao;
import com.learning.project.model.Item;
import com.learning.project.model.Project;
import com.learning.project.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Administrator on 03-12-2019.
 */

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserDao userDao;
    @Autowired
    private ProjectDao projectDao;


    @Override
    public List<Item> getAllItems() {
        List<Item> allItems = itemDao.getAllItems();
        return allItems.stream()
                .map(item -> {
                    User user = userDao.getUserById(item.getCreatedBy());
                    item.setCreatedByName(user.getName());
                    Project project = projectDao.getProjectById(item.getProjectLinked());
                    item.setProjectLinkedByName(project.getName());
                    return item;

                })
                .collect(Collectors.toList());
    }

    @Override
    public int addItem(Item item) {

        return itemDao.addItem(item);
    }

    @Override
    public Item getItemById(int id) {
        return itemDao.getItemById(id);
    }

    @Override
    public boolean updateItem(Item item) {
        return itemDao.updateItem(item);
    }

    @Override
    public boolean deleteItemById(int id) {
        return itemDao.deleteItemById(id);
    }
}
