package com.learning.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BugTrackerFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(BugTrackerFinalApplication.class, args);
	}

}
