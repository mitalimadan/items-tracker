package com.learning.project.model;

import java.sql.Date;

/**
 * Created by Lucky  on 09/05/2020.
 */
public class Item {

    private int id;
    private String name;
    private String description;
    private int parent;
    private int type;
    private int priority;
    private Date createdAt;
    private int createdBy;
    private float estimatedTime;
    private int projectLinked;
    private String createdByName;
    private String projectLinkedByName;

    public String getProjectLinkedByName() {
        return projectLinkedByName;
    }

    public void setProjectLinkedByName(String projectLinkedByName) {
        this.projectLinkedByName = projectLinkedByName;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public Item(int id, String name, String description, int parent, int type, int priority, Date createdAt, int createdBy, float estimatedTime, int linkedProject, String createdByName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.parent = parent;
        this.type = type;
        this.priority = priority;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.estimatedTime = estimatedTime;
        this.projectLinked = projectLinked;
    }

    public Item() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public float getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(float estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getProjectLinked() {
        return projectLinked;
    }

    public void setProjectLinked(int projectLinked) {
        this.projectLinked = projectLinked;
    }
}
