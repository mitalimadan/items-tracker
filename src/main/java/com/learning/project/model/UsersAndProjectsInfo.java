package com.learning.project.model;

import java.util.List;

/**
 * Created by Lucky  on 12/05/2020.
 */
public class UsersAndProjectsInfo {
    private List<User> user;
    private List<Project> project;

    public void setUser(List<User> user) {
        this.user = user;
    }

    public void setProject(List<Project> project) {
        this.project = project;
    }

    public List<User> getUser() {
        return user;
    }

    public List<Project> getProject() {
        return project;
    }
}
