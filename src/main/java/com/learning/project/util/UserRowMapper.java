package com.learning.project.util;

import com.learning.project.model.User;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by Lucky  on 12/05/2020.
 */
public class UserRowMapper {

    public final static RowMapper<User> USER_ROW_MAPPER = (rs, rownum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));
        user.setType(rs.getInt("type"));
        user.setPassword(rs.getString("password"));
        user.setEmail(rs.getString("email"));

        return user;
    };
}
