package com.learning.project.util;

import com.learning.project.model.Item;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by Administrator on 11-11-2019.
 */
public class Utility {
    public final static String ITEM = "items";
    public final static RowMapper<Item> ITEM_ROW_MAPPER = (rs, rownum) -> {
        Item item = new Item();
        item.setId(rs.getInt("id"));
        item.setName(rs.getString("name"));
        item.setDescription(rs.getString("description"));
        item.setParent(rs.getInt("parent"));
        item.setType(rs.getInt("type"));
        item.setPriority(rs.getInt("priority"));
        item.setCreatedAt(rs.getDate("created_at"));
        item.setCreatedBy(rs.getInt("created_by"));
        item.setEstimatedTime(rs.getFloat("estimate_time"));
        item.setProjectLinked(rs.getInt("project_linked"));

        return item;
    };

}
