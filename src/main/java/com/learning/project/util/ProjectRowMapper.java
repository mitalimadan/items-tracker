package com.learning.project.util;

import com.learning.project.model.Project;
import com.learning.project.model.User;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by Lucky  on 12/05/2020.
 */
public class ProjectRowMapper {

    public final static RowMapper<Project> PROJECT_ROW_MAPPER = (rs, rownum) -> {
        Project project = new Project();
        project.setId(rs.getInt("id"));
        project.setName(rs.getString("name"));
        project.setDescription(rs.getString("description"));
        project.setOwner(rs.getInt("owner"));
        project.setStartedAt(rs.getDate("started_at"));
        project.setStatus(rs.getInt("status"));

        return project;
    };
}
