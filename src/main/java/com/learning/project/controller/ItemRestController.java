package com.learning.project.controller;

import com.learning.project.dao.ProjectDao;
import com.learning.project.dao.UserDao;
import com.learning.project.model.Item;
import com.learning.project.model.UsersAndProjectsInfo;
import com.learning.project.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 03-12-2019.
 */

@RestController
public class ItemRestController {

    @Autowired
    private ItemService itemService;
    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserDao userDao;


    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public List<Item> getAllItems() {

        List<Item> allItems = itemService.getAllItems();
        return allItems;
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public int addItem(@RequestBody Item item) {

        return itemService.addItem(item);
    }

    @RequestMapping(value = "/items/{id}", method = RequestMethod.GET)
    public Item getItemById(@PathVariable int id) {
        return itemService.getItemById(id);
    }

    @RequestMapping(value = "/items", method = RequestMethod.PUT)
    public boolean updateItem(@RequestBody Item item) {
        return itemService.updateItem(item);
    }

    @RequestMapping(value = "/items/{id}", method = RequestMethod.DELETE)
    public void deleteItemById(@PathVariable int id) {
        itemService.deleteItemById(id);
    }


    @RequestMapping(value = "/usersAndProjects", method = RequestMethod.GET)
    public UsersAndProjectsInfo getUsersAndProjectsInfo() {
        UsersAndProjectsInfo usersAndProjectsInfo = new UsersAndProjectsInfo();
        usersAndProjectsInfo.setProject(projectDao.getAllProjects());
        usersAndProjectsInfo.setUser(userDao.getAllUsers());
        return usersAndProjectsInfo;
    }


}
